/*
ProductCtrl.js
Created by Jon D.
Controller object that handles all of the Product calls to interact with the html frontend.
*/

(function(app) {
    var ProductCtrl = function($scope, $modal, RESTFactory, $filter) {

        var baseurl = 'webresources/product';

        
        var init = function() {

            //load data for page from WEB api
            $scope.status = 'Loading Products...';
            $scope.products = RESTFactory.restCall('get', baseurl, -1, '').then(function(products) {

                if (products.length > 0) { //object returned it worked, number indicates status
                    $scope.products = products;                   
                    $scope.status = 'Products Retrieved';
                }
                else {
                    $scope.status = 'Products not retrieved code = ' + products;
                }
            }, function(reason) { //error            
                $scope.status = 'Products not retrieved ' + reason;
            });
            $scope.product = $scope.products[0];
        };//init
        
        $scope.findSelected = function(col, order){
          $scope.products = $filter('orderby')($scope.products, col, order);  
        };
        

        //selectRow function - user selects row for modal to update or delete
        $scope.selectRow = function(row, product) {
            if (row < 0) {
                $scope.todo = 'add';
                $scope.product = new Object();
            }
            else {
                $scope.product = product;
                $scope.selectedRow = row;
                $scope.todo = 'update';
            }
          
            var modalInstance = $modal.open({
                templateUrl: 'partials/productModal.html',
                controller: 'ProductModalCtrl',
                scope: $scope,
                backdrop: 'static'
            });
            
            //modal returns results here
            modalInstance.result.then(function(results) {
                switch (results.operation) {
                    case 'add':
                        if (results.numOfRows === 1) {
                            $scope.status = 'Product ' + $scope.product.productcode + ' Added!';
                            $scope.selectedRow = $scope.products.length - 1;
                        }
                        else {
                            $scope.status = 'Product Not Added!';
                        }
                        break;
                    case 'delete':
                        for(var i = 0; i < $scope.products.length; i++){
                            if($scope.products[i].productcode === results.productcode){
                                $scope.products.splice(i, 1);
                                break;
                            }
                        }
                        if (results.productcode !== null) {
                            $scope.selectedRow = null;
                            $scope.status = 'Product ' + results.productcode + ' Deleted!';
                        }
                        else {
                            $scope.status = 'Product ' + results.productcode + ' Not Deleted!';
                        }
                        break;
                    
                    case 'update':
                        if (results.productcode !== null) {
                            $scope.status = 'Product ' + results.productcode + ' Updated!';                           
                        }
                        else {
                            $scope.status = 'Product Not Updated!';
                        }
                        break;
                    case 'cancel':
                        $scope.status = 'Product Not Updated!';
                        break;

                }


            });


        };//selectRow
        init();
    };
    app.controller('ProductCtrl', ['$scope', '$modal', 'RESTFactory', '$filter', ProductCtrl]);
})(angular.module('case1'));



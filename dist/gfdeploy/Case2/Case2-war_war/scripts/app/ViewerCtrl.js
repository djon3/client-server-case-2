/*
ViewerCtrl.js
Created by Jon D.
Controller object that handles all of the purchase order calls to interact with the html frontend.
*/

(function(app) {
    var ViewerCtrl = function($scope, $modal, RESTFactory, $filter) {
        
        var selectedVendor = "";
        
        //Initialization function - initializes the product modal with values
        var init = function() {
            
            $scope.status = 'Loading vendors...';
            $scope.vendors = RESTFactory.restCall('get', 'webresources/vendor', -1, '').then(function(vendors) {

                if (vendors.length > 0) { //object returned it worked, number indicates status
                    $scope.vendors = vendors;
                    $scope.status = 'Vendors Retrieved';
                }
                else {
                    $scope.status = 'Vendors not retrieved code = ' + vendors;
                }
            }, function(reason) { //error            
                $scope.status = 'Vendors not retrieved ' + reason;
            });
            $scope.vendor = $scope.vendors[0];

        };//init   
        
        //getPO - after selecting the vendorno, this retreives the purchase order numbers for the selected vendor.
        $scope.getPO = function(vendorno){
            $scope.status = 'Loading Purchase Orders...';
            $scope.purchaseorders = RESTFactory.restCall('get', 'webresources/po', vendorno, '').then(function(purchaseorders) {

                if (purchaseorders.length > 0) { //object returned it worked, number indicates status
                    $scope.purchaseorders = purchaseorders;
                    $scope.status = 'Purchase Orders Retrieved';
                }
                else {
                    $scope.status = 'Purchase Orders not retrieved code = ' + purchaseorders;
                }
            }, function(reason) { //error            
                $scope.status = 'Purchase Orders not retrieved ' + reason;
            });
            $scope.purchaseorders = $scope.purchaseorders[0];
        };
        
        //getPO - after selecting the vendorno, this retreives the purchase order numbers for the selected vendor.
        $scope.getPOLI = function(){
            $scope.status = 'Loading Purchase Order Line Items...';
            $scope.purchaseorderslineitems = RESTFactory.restCall('get', 'webresources/po', -1, '').then(function(purchaseorderslineitems) {

                if (purchaseorderslineitems.length > 0) { //object returned it worked, number indicates status
                    $scope.purchaseorderslineitems = purchaseorderslineitems;
                    $scope.status = 'Purchase Order Line Items Retrieved';
                }
                else {
                    $scope.status = 'Purchase Order Line Items not retrieved code = ' + purchaseorderslineitems;
                }
            }, function(reason) { //error            
                $scope.status = 'Purchase Order Line Items not retrieved ' + reason;
            });
            $scope.purchaseorderslineitems = $scope.purchaseorderslineitems[0];
        };

        //changeVendor function - Changes multiple values when the vendor id has been changed
        $scope.changeVendor = function() {
            var getVendId = document.getElementById("vendernoid");
            selectedVendor = getVendId.options[getVendId.selectedIndex].text;
            $scope.status = "Wait...";
            $scope.pickedVendor = true;
            $scope.getPO(parseInt(selectedVendor));
            $scope.getPOLI();
            
           // $scope.pickedPO = false;
//            $scope.total = 0.0;
//            $scope.tax = 0.0;
//            $scope.extended = 0.0;
//            $scope.sub = 0.0;
//            $scope.hasItems = false;
//            $scope.generated = false;
//            $scope.items = [];
//            productChosen.selectedIndex = -1;
//            qty.selectedIndex = -1;
        }; //changeVendor

        var chosenProd;
        $scope.items = [];

        $scope.pickedPO = function(){
            
            $scope.pickedPO = false;
            $scope.code = $scope.products[document.getElementById("productChosen").value].prodcd;
            $scope.name = $scope.purchaseorderslineitems[document.getElementById("ponum").value].productName;
            $scope.qty = $scope.purchaseorderslineitems[document.getElementById("ponum").value].qty;
            $scope.price = $scope.purchaseorderslineitems[document.getElementById("ponum").value].extPrice;
            $scope.ex = ($scope.price * $scope.qty);
            
            $scope.status = 'PO ' + $scope.ponum + " - " + $scope.purchaseorders[document.getElementById("ponum").value].podate;
          
            $scope.items.push({
               code:  $scope.code,
               name:  $scope.name,      
               qty:   $scope.qty,
               price: $scope.price,             
               ex:   $scope.ex
            });

            $scope.sub += $scope.ex * $scope.qty;
            $scope.tax = $scope.sub * 0.13;
            $scope.total = $scope.sub + $scope.tax;
            
        };
        
        //allFieldsChosen function - Chooses all fields for the product
        $scope.allFieldsChosen = function() {
            return $scope.productChosen;
        };


        var poNumber = 0;
        //createPO function - Controller method to create PO
        $scope.createPO = function() {
            $scope.status = "Wait...";
            var PODTO = new Object();
            PODTO.vendorno = selectedVendor; //$scope.vendor.vendorno; //might have to change
            PODTO.ponum = $scope.products[document.getElementById("productChosen").value].vendorno;
            PODTO.prodcd = $scope.productcode;
            PODTO.qty = $scope.quantity;
            PODTO.amount = $scope.total;
            PODTO.items = $scope.items;

            $scope.PO = RESTFactory.restCall('post', 'webresources/po',
                    selectedVendor,
                    PODTO).then(function(results) {
                        
                if (results.length > 0) {
                    $scope.status = results;
                    $scope.notcreated = false;
                    poNumber = results.match(/\d+/);
                }
                else {
                    $scope.status = 'PO not created - ' + results;
                }

            }, function(reason) { //error
                $scope.status = 'PO not created - ' + reason;
            });
        };//creatPO
        
        //changeAddPDFButton function - toggle the pdf button after the add PO has been made
        $scope.changeAddPDFButton = function(){
            $scope.togglePDF = false;
        };
        
       
       //viewPdf function - Sends user to the pdf page of the new PO
        $scope.viewPdf = function(){
            var hostUrl = window.location.host;
            window.location.href = 'http://' + hostUrl +'/Case1/POPDF?po=' + poNumber;
        };
        
        init();
    };

    app.controller('ViewerCtrl', ['$scope', '$modal', 'RESTFactory', '$filter', ViewerCtrl]);
})(angular.module('case1'));





/*
Directive.js
Created by Jon D.
Object that handles the delete call asking if the user really wants to delete.
*/

(function(app){
    app.directive('confirmClick', function(){
       return{
           restrict: 'A',
           priority: 0,
           terminal: true,
           link: function(scope, element, attr){
               var msg = attr.confirmationNeeded || "Really Delete?";
               var clickAction = attr.ngClick;
               element.bind('click', function(){
                  if(window.confirm(msg)){
                      scope.$apply(clickAction);
                  } 
               });
           }
       };
    });
    
    
}(angular.module('case1')));



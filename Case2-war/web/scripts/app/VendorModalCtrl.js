/*
VendorModalCtrl.js
Created by Jon D.
Modal Controller object that handles all of the vendor calls to interact with the html frontend.
*/

(function(app) {

    var VendorModalCtrl = function($scope, $modalInstance, RESTFactory) {
        var baseurl = 'webresources/vendor';
        var retVal = {operation: '', vendorno: -1, numOfRows: -1};
        
        //Initialization function - initializes the product modal with values
        var init = function() {
            if ($scope.todo === 'add') {
                $scope.modalTitle = 'Add Vendor';
            } else {
                $scope.modalTitle = 'Vendor: ' + $scope.vendor.vendorno;
            }
        }; //init

        //add function - adds a vendor from the database to the html vendor table 
        $scope.add = function() {
            RESTFactory.restCall('post', baseurl, -1, $scope.vendor).then(function(results) {
                if (results.substring) { //string returned it worked, number indicates status
                    if (parseInt(results) > 0) {
                        $scope.vendor.vendorno = parseInt(results);
                        $scope.vendors.push($scope.vendor);
                        retVal.numOfRows = 1;
                        retVal.operation = 'add';
                        retVal.vendorno = parseInt(results);
                        $modalInstance.close(retVal);
                    }
                    else {
                        retVal = 'Vendor was not added! - system error ' + results;
                        $modalInstance.close(parseInt(results));
                    }
                }
                else {
                    retVal = 'Vendor was not added! - system error ' + reason;
                    $modalInstance.close(parseInt(results));
                }
            }, function(reason) { //error
                retVal = 'Vendor was not added! - system error ' + reason;
                $modalInstance.close(retVal);

            });
        }; //$scope.add
        
         //update function - updates the selected vendor in the database and the html table
        $scope.update = function() {
            RESTFactory.restCall('put', baseurl, -1, $scope.vendor).then(function(results) {
                retVal.operation = 'update';
                retVal.vendorno = $scope.vendor.vendorno;

                if (results.substring) {
                    retVal.numOfRows = parseInt(results);
                } else {
                    retVal.numOfRows = -1;
                }

                $modalInstance.close(retVal);
            }, function(reason) {
                retVal = 'Vendor not updated! - system error ' + reason;
                $modalInstance.close(retVal);
            });
        }; //$scope.update
        
        //delete function - deletes the selected vendor from the database and the html table
        $scope.delete = function() {
            RESTFactory.restCall('delete', baseurl, $scope.vendor.vendorno, '').then(function(results) {
                retVal.operation = 'delete';
                retVal.vendorno = $scope.vendor.vendorno;

                if (results.substring) { //string=worked, number=problem
                    retVal.numOfRows = parseInt(results);
                }
                else {
                    retVal.numOfRows = -1;
                }

                $modalInstance.close(retVal);
            }, function() { //error
                retVal.numOfRows = -1;
                $modalInstance.close(retVal);
            });
        };

        //cancel function - cancels and closes the current modal and cancels all changes
        $scope.cancel = function() {
            $modalInstance.close(-1);
        };
        
        //close function - closes the product modal
        $scope.close = function() {
            $modalInstance.close(1);
        };

        init();
    };

    app.controller('VendorModalCtrl', ['$scope', '$modalInstance', 'RESTFactory', VendorModalCtrl]);
})(angular.module('case1'));




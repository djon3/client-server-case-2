/*
GeneratorCtrl.js
Created by Jon D.
Controller object that handles all of the generator calls to interact with the html frontend.
*/

(function(app) {
    var GeneratorCtrl = function($scope, $modal, RESTFactory, $filter) {

        var baseurl = 'webresources/vendor';
        var selectedVendor = "";

        //Initialization function - initializes the product modal with values
        var init = function() {
            $scope.status = 'Loading vendors...';
            $scope.vendors = RESTFactory.restCall('get', baseurl, -1, '').then(function(vendors) {

                if (vendors.length > 0) { //object returned it worked, number indicates status
                    $scope.vendors = vendors;
                    $scope.status = 'Vendors Retrieved';
                }
                else {
                    $scope.status = 'Vendors not retrieved code = ' + vendors;
                }
            }, function(reason) { //error            
                $scope.status = 'Vendors not retrieved ' + reason;
            });
            $scope.vendor = $scope.vendors[0];

        };//init   

        //changeVendor function - Changes multiple values when the vendor id has been changed
        $scope.changeVendor = function() {
            var getVendId = document.getElementById("vendernoid");
            selectedVendor = getVendId.options[getVendId.selectedIndex].text;
            $scope.status = "Wait...";
            $scope.pickedVendor = true;
            $scope.getVendorProducts(parseInt(selectedVendor));
            $scope.pickedProduct = false;
            $scope.total = 0.0;
            $scope.tax = 0.0;
            $scope.extended = 0.0;
            $scope.sub = 0.0;
            $scope.hasItems = false;
            $scope.generated = false;
            $scope.items = [];
            productChosen.selectedIndex = -1;
            qty.selectedIndex = -1;
        }; //changeVendor

        //getVendorProducts function - get all products for selected vendor
        $scope.getVendorProducts = function(vendorno) {  

            $scope.products = RESTFactory.restCall('get', 'webresources/product', -1, vendorno).then(function(product) {

                if (product.length > 0) { //object returned it worked, number indicates status
                    $scope.products = product;
                    $scope.status = 'Products Retrieved';
                }
                else {
                    $scope.status = 'Products not retrieved code = ' + product;
                }
            }, function(reason) { //error            
                $scope.status = 'Products not retrieved ' + reason;
            });
            $scope.product = $scope.products[0];

        };
        
        //allFieldsChosen function - Chooses all fields for the product
        $scope.allFieldsChosen = function() {
            return $scope.productChosen;
        };

        var chosenProd;
        $scope.items = [];
        
        //addProduct function - Adds a new product to the cart
        $scope.addProduct = function() {
            $scope.status = 'Item Added!';
            $scope.ex = $scope.products[document.getElementById("productChosen").value].MSRP;
            $scope.items.push({
                ponum: $scope.products[document.getElementById("productChosen").value].Vendorno,
                product: $scope.productcode,
                productCode: $scope.productcode,
                qty: $scope.quantity,
                ex: ($scope.ex * $scope.quantity)
            });

            $scope.sub += $scope.ex * $scope.quantity;
            $scope.tax = $scope.sub * 0.13;
            $scope.total = $scope.sub + $scope.tax;

        };

        var poNumber = 0;
        //createPO function - Controller method to create PO
        $scope.createPO = function() {
            $scope.status = "Wait...";
            var PODTO = new Object();
            PODTO.vendorno = selectedVendor; //$scope.vendor.vendorno; //might have to change
            PODTO.ponum = $scope.products[document.getElementById("productChosen").value].vendorno;
           // PODTO.prodcd = $scope.productcode;
            //PODTO.qty = $scope.quantity;
            PODTO.amount = $scope.total;
            PODTO.items = $scope.items;

            $scope.PO = RESTFactory.restCall('post', 'webresources/po',
                    selectedVendor,
                    PODTO).then(function(results) {
                        
                if (results.length > 0) {
                    $scope.status = results;
                    $scope.notcreated = false;
                    poNumber = results.match(/\d+/);
                }
                else {
                    $scope.status = 'PO not created - ' + results;
                }

            }, function(reason) { //error
                $scope.status = 'PO not created - ' + reason;
            });
        };//creatPO
        
        //changeAddPDFButton function - toggle the pdf button after the add PO has been made
        $scope.changeAddPDFButton = function(){
            $scope.togglePDF = false;
        };
        
 
        //removeItem function - Removes selected item from the items array
        $scope.removeItem = function(){
             var selectedQty = document.getElementById("qty").value;                      
             if(selectedQty === "0"){
                var selectedProduct = document.getElementById("productChosen").value; 
                $scope.items.splice(selectedProduct, 1);               
                $scope.updateCartTotal();
             }
        };
        
        //updateCartTotal function - Updates the totals in the cart after an item has been deleted
        $scope.updateCartTotal = function () {
          $scope.sub = 0;
          for (var i = 0; i < $scope.items.length; ++i) {
                $scope.sub = $scope.sub + ($scope.items[i].qty * $scope.items[i].costprice);
          }
          $scope.tax = $scope.sub * 0.13;
          $scope.total = $scope.tax + $scope.sub;
        };
       
       //viewPdf function - Sends user to the pdf page of the new PO
        $scope.viewPdf = function(){
            var hostUrl = window.location.host;
            window.location.href = 'http://' + hostUrl +'/Case1/POPDF?po=' + poNumber;
        };
        
        init();
    };

    app.controller('GeneratorCtrl', ['$scope', '$modal', 'RESTFactory', '$filter', GeneratorCtrl]);
})(angular.module('case1'));


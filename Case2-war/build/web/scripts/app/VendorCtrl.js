/*
Vendor.js
Created by Jon D.
Controller object that handles all of the vendor calls to interact with the html frontend.
*/

(function(app) {
    var VendorCtrl = function($scope, $modal, RESTFactory, $filter) {

        var baseurl = 'webresources/vendor';
        
        //Initialization function - initializes the product modal with values
        var init = function() {

            //load data for page from WEB api
            $scope.status = 'Loading Vendors...';
            $scope.vendors = RESTFactory.restCall('get', baseurl, -1, '').then(function(vendors) {

               if (vendors.length > 0) { //object returned it worked, number indicates status
                    $scope.vendors = vendors;
                    $scope.status = 'Vendors Retrieved';
               }
               else {
                   $scope.status = 'Vendors not retrieved code = ' + vendors;
               }
            }, function(reason) { //error            
                $scope.status = 'Vendors not retrieved ' + reason;
            });
            $scope.vendor = $scope.vendors[0];
        };//init
        
         $scope.findSelected = function(col, order){
          $scope.products = $filter('orderby')($scope.vendors, col, order);  
        };

        //selectRow function - user selects row for modal to update or delete
        $scope.selectRow = function(row, vendor) {
            if (row < 0) {
                $scope.todo = 'add';
                $scope.vendor = new Object();
            }
            else {
                $scope.vendor = vendor;
                $scope.selectedRow = row;
                $scope.todo = 'update';
            }
          
            var modalInstance = $modal.open({
                templateUrl: 'partials/vendorModal.html',
                controller: 'VendorModalCtrl',
                scope: $scope,
                backdrop: 'static'
            });          
            
            //modal retuns results here
            modalInstance.result.then(function(results) {
                switch (results.operation) {
                    case 'add':
                        if (results.numOfRows === 1) {
                            $scope.status = 'Vendor ' + results.vendorno + ' Added!';
                            $scope.selectedRow = $scope.vendors.length - 1;
                        }
                        else {
                            $scope.status = 'Vendor Not Added!';
                        }
                        break;
                    case 'delete':
                        for(var i = 0; i < $scope.vendors.length; i++){
                            if($scope.vendors[i].vendorno === results.vendorno){
                                $scope.vendors.splice(i, 1);
                                break;
                            }
                        }
                        if (results.numOfRows === 0) {
                            $scope.selectedRow = null;
                            $scope.status = 'Vendor ' + results.vendorno + ' Deleted!';
                        }
                        else {
                            $scope.status = 'Vendor ' + results.vendorno + ' Not Deleted!';
                        }
                        break;
                    
                    case 'update':
                        if (results.numOfRows === 1) {
                            $scope.status = 'Vendor ' + results.vendorno + ' Updated!';                           
                        }
                        else {
                            $scope.status = 'Vendor Not Updated!';
                        }
                        break;
                    case 'cancel':
                        $scope.status = 'Vendor Not Updated!';
                        break;

                }


            });


        };//selectRow
        init();
    };
    app.controller('VendorCtrl', ['$scope', '$modal', 'RESTFactory', '$filter', VendorCtrl]);
})(angular.module('case1'));








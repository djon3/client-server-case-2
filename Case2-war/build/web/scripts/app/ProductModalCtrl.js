/*
ProductModalCtrl.js
Created by Jon D.
Modal Controller object that handles all of the generator calls to interact with the html frontend.
*/

(function(app) {

    var ProductModalCtrl = function($scope, $modalInstance, RESTFactory) {
        var baseurl = 'webresources/product';
        var retVal = {operation: '', productcode: -1, numOfRows: -1};
        $scope.isDisabled = false;
        
        //Initialization function - initializes the product modal with values
        var init = function() {
            $scope.vendors = RESTFactory.restCall('get', 'webresources/vendor', -1, '').then(function(vendors) {

                if (vendors.length > 0) { //object returned it worked, number indicates status
                    $scope.vendors = vendors;
                }
            });
            $scope.undo = new Object();            
            if ($scope.todo === 'add') {                
                $scope.modalTitle = 'Add Product';
                $scope.isDisabled = false;
            } else {
                $scope.modalTitle = 'Product: ' + $scope.product.productcode;
                $scope.vendorno = $scope.product.vendorno;
                $scope.isDisabled = true;
                
                angular.copy($scope.product, $scope.undo);
            }
        }; //init

        //$scope.products = [];
        
        //add function - adds a product from the database to the html product table 
        $scope.add = function() {

            RESTFactory.restCall('post', 'webresources/product', -1, $scope.product).then(function(results) {
                if (results.substring) { //string returned it worked, number indicates status
                    if (parseInt(results) >= 0) {
                        //$scope.product.productcode = String(results);                        
                        $scope.products.push($scope.product);
                        retVal.numOfRows = 1;
                        retVal.operation = 'add';
                        retVal.productcode = results;                        
                        $modalInstance.close(retVal);
                    }
                    else {
                        retVal = 'Product was not added! - system error ' + results;
                        $modalInstance.close(parseInt(results));
                    }
                }
                else {
                    retVal = 'Product was not added! - system error ' + reason;
                    $modalInstance.close(parseInt(results));
                }
            }, function(reason) { //error
                retVal = 'Product was not added! - system error ' + reason;
                $modalInstance.close(retVal);

            });
        }; //$scope.add

        //update function - updates the selected product in the database and the html table
        $scope.update = function() {
            RESTFactory.restCall('put', baseurl, -1, $scope.product).then(function(results) {
                retVal.operation = 'update';
                retVal.productcode = $scope.product.productcode;

                if (results.substring) {
                    retVal.numOfRows = parseInt(results);
                } else {
                    retVal.numOfRows = -1;
                }

                $modalInstance.close(retVal);
            }, function(reason) {
                retVal = 'Product not updated! - system error ' + reason;
                $modalInstance.close(retVal);
            });
        }; //$scope.update

        //delete function - deletes the selected product from the database and the html table
        $scope.delete = function() {
            RESTFactory.restCall('delete', baseurl, $scope.product.productcode, '').then(function(results) {
                retVal.operation = 'delete';
                retVal.productcode = $scope.product.productcode;

                if (results.substring) { //string=worked, number=problem
                    retVal.numOfRows = parseInt(results);
                }
                else {
                    retVal.numOfRows = -1;
                }

                $modalInstance.close(retVal);
            }, function() { //error
                retVal.numOfRows = -1;
                $modalInstance.close(retVal);
            });
        };

        //cancel function - cancels and closes the current modal and cancels all changes
        $scope.cancel = function() {
            retVal.operation = 'cancel';
            retVal.productcode = $scope.product.productcode;
            retVal.numOfRows = -1;
            if($scope.todo === 'update'){
                for(var i = 0; i < $scope.products.length; i++){
                    if($scope.products[i].productcode === $scope.undo.productcode){
                        angular.copy($scope.undo, $scope.products[i]);
                        angular.copy($scope.undo, $scope.product);
                        break;
                    }
                }
            }
            
            
            $modalInstance.close(-1);
        };

        //close function - closes the product modal
        $scope.close = function() {
            $modalInstance.close(1);
        };

        init();
    };

    app.controller('ProductModalCtrl', ['$scope', '$modalInstance', 'RESTFactory', ProductModalCtrl]);
})(angular.module('case1'));







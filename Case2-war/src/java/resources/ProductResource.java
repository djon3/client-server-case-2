/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import case2ejbs.ProductFacadeBeans;
import dtos.ProductEJBDTO;
import dtos.ProductsDTO;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import models.ProductsModal;

/*
 *  ProductResource.java
 *
 *  Purpose:    Contains methods for supporting db access for purchase order information.              
 *  Author:     Jon Decher
 *  Revisions: 
 */
@Path("product")
@RequestScoped
public class ProductResource {
    
    @EJB
    ProductFacadeBeans pfb;

    @Context
    private UriInfo context;

    //resource needs to be already defined in Glassfish    
    DataSource ds;

    /**
     * Creates a new instance of ProductResource
     */
    public ProductResource() {
    }

    /* createProductFromJson - creates a product from json data
       @param a value of ProductsDTO
       @return Response
    */
    @POST
    @Consumes("application/json")
    public Response createProductFromJson(ProductEJBDTO product) {
        int id = pfb.addProduct(product);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(id).build();
    }
    
    /* getProductsJson - gets products from json data
       @param no params
       @return an array list of ProductDTO
    */
    @GET
    @Produces("application/json")
    public ArrayList<ProductEJBDTO> getProductsJson() {
        ArrayList<ProductEJBDTO> products = pfb.getAllProducts();
        return products;
    }
    
    /* getVendorProductsJson - gets vendor products from json data
       @param a PathParam and an integer
       @return an arraylist of ProductsDTO
    */
    @GET
    @Path("/{vendorno}")
    @Produces("application/json")
    public ArrayList<ProductsDTO> getVendorProductsJson(@PathParam("vendorno") int vendorno){
        ProductsModal model = new ProductsModal();
        return model.getAllProductsForVendor(vendorno, ds);
    }
    
    
    @GET
    @Path("products/{vendorno}")
    @Produces("application/json")
    public List<ProductEJBDTO>getProductForVendorJson(@PathParam("vendorno") int vendorno){
        return pfb.getAllProductsForVendor(vendorno);
    }
    
    /* updateProductFromJson - updates products from json data
       @param a product of type ProductsDTO
       @return a response
    */
    @PUT
    @Consumes("application/json")
    public Response updateProductFromJson(ProductEJBDTO product) {
        String numOfRowsUpdated = pfb.updateProduct(product);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsUpdated).build();
    }
    
    /* deleteProductfromJson - deletes a product from json data
       @param a PathParam and a string
       @return a response
    */  
    @DELETE
    @Path("/{productcode}")
    @Consumes("application/json")
    public Response deleteProductFromJson(@PathParam("productcode")String productcode){
        String numOfRowsDeleted = pfb.deleteProduct(productcode);
        URI uri = context.getAbsolutePath();
        System.out.println("number of rows deleted " + numOfRowsDeleted);
        return Response.created(uri).entity(numOfRowsDeleted).build();
    }

   
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package resources;

import case2ejbs.POFacadeBean;
import dtos.PurchaseOrderEJBDTO;
import dtos.PurchaseOrderLineitemEJBDTO;
import java.net.URI;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/*
 *  POResource.java
 *
 *  Purpose:    Contains methods for supporting db access for purchase order information.              
 *  Author:     Jon Decher
 *  Revisions: 
 */
@Path("po")
public class POResource {
    @EJB
    POFacadeBean pofb;

    @Context
    private UriInfo context;

    //resource needs to be already defined in Glassfish
    //@Resource(lookup = "jdbc/Info5059db")
    DataSource ds;
    
    /**
     * Creates a new instance of POResource
     */
    public POResource() {
    }
    
    /* createPO - creates a purchase order
       @param a value of type PurchaseOrderDTO
       @return a response
    */
    @POST
    @Consumes("application/json")
    public Response createPO(PurchaseOrderEJBDTO po){        
        String msg = pofb.addPO(po);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(msg).build();
    }
    
    @GET//@Path("po/{vendorno}") @PathParam("vendorno")    
    @Path("/{vendorno}")
    @Produces("application/json")
    public ArrayList<PurchaseOrderEJBDTO>getPOForVendorJson(@PathParam("vendorno") int vendorno){ 
        ArrayList<PurchaseOrderEJBDTO> po = pofb.getAllPOsForVendor(vendorno);
        return po;
    }
    
    @GET  
    @Produces("application/json")
    public ArrayList<PurchaseOrderLineitemEJBDTO>getPOLI(){ 
        ArrayList<PurchaseOrderLineitemEJBDTO> po = pofb.getAllPOLIsForVendor();
        return po;
    }
    
    
  

}

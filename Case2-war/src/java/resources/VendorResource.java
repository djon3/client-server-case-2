package resources;

import case2ejbs.VendorFacadeBeans;
import dtos.VendorDTO;
import dtos.VendorEJBDTO;
import java.net.URI;
import java.util.ArrayList;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import models.VendorModal;

/*
 *  VendorResource.java
 *
 *  Purpose:    Contains methods for supporting db access for vendor information. *             
 *  Author:     Jon Decher
 *  Revisions:  Processes database calls through the ejb facade and entity beans 
 *              instead of JDBC models.
 */

@Path("vendor")
@RequestScoped
public class VendorResource {
    
    @EJB
    private VendorFacadeBeans vfb;

    @Context
    private UriInfo context;

    //resource needs to be already defined in Glassfish
  // @Resource(lookup = "jdbc/Info5059db")
    DataSource ds;

    /**
     * Creates a new instance of VendorResource
     */
    public VendorResource() {
    }

    /* createVendorFromJson - creates a vendor from json data
       @param a value of VendorEJBDTO
       @return Response representing the added row
    */
    @POST
    @Consumes("application/json")
    public Response createVendorFromJson(VendorEJBDTO vendor) {       
        int id = vfb.addVendor(vendor);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(id).build();
    }
    

    /* getVendorsJson - gets vendors from json data
       @param no params
       @return a list of VendorEJBDTO
    */
    @GET    
    @Produces("application/json")
    public List<VendorEJBDTO> getVendorsJson() {
        List<VendorEJBDTO> vendors = vfb.getVendors();
        return vendors;
    }
    

    /* updateVendorFromJson - updates vendors from json data
       @param vendor dto representing a single vendor for update
       @return a response indicating number of rows updated
    */
    @PUT
    @Consumes("application/json")
    public Response updateVendorFromJson(VendorEJBDTO vendor) {       
        int numOfRowsUpdated = vfb.updateVendor(vendor);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsUpdated).build();
    }
    
    
    /* deleteVendorfromJson - deletes a vendor from json data
       @param a PathParam and a string
       @return a response indicating the deleted row
    */ 
    @DELETE
    @Path("/{vendorno}")
    @Consumes("application/json")
    public Response deleteVendorFromJson(@PathParam("vendorno")int vendorno){       
        int numOfRowsDeleted = vfb.deleteVendor(vendorno);
        URI uri = context.getAbsolutePath();
        System.out.println("number of rows deleted " + numOfRowsDeleted);
        return Response.created(uri).entity(numOfRowsDeleted).build();
    }

    
}

package models;

import dtos.ProductsDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

/*
 * ProductsModal.java
 *
 *  Purpose:    Contains methods for supporting db access for product information
 *              Usually consumed by the ViewModel Class via DTO
 *  Author:     Jon Decher
 *  Revisions:  None
 */
@Named(value = "productsModal")
@RequestScoped
public class ProductsModal implements Serializable {

    public ProductsModal() {
    }

    /* addProduct - inserts values into the product table in the database
     @param a ProductsDTO and a DataSource
     @return an int containing the vendor number
    */
    public int addProduct(ProductsDTO details, DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        int vendorno = 0;

        String sql = "INSERT INTO Products (Vendorno,Productcode,Vendorsku,Productname,"
                + "Costprice,MSRP,Rop,Eoq,Qoh,Qoo) "
                + " VALUES (?,?,?,?,?,?,?,?,?,?)";

        try {
            System.out.println("before get connection");
            con = ds.getConnection();
            System.out.println("after get connection");
            pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, details.getVendorno());
            pstmt.setString(2, details.getProductcode());
            pstmt.setString(3, details.getVendorsku());
            pstmt.setString(4, details.getProductname());
            pstmt.setDouble(5, details.getCostprice());
            pstmt.setDouble(6, details.getMSRP());
            pstmt.setInt(7, details.getRop());
            pstmt.setInt(8, details.getEoq());
            pstmt.setInt(9, details.getQoh());
            pstmt.setInt(10, details.getQoo());
            pstmt.execute();

            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                rs.next();
                vendorno = rs.getInt(1);
            }
            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return vendorno;
    }
    
    /* getProducts - retrieves product items from the database
     @param a DataSource
     @return an arraylist of PurchaseOrderLineitmDTO
    */
    public ArrayList<ProductsDTO> getProducts(DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        ArrayList<ProductsDTO> productArray = new ArrayList<>();

        String sql = "SELECT * FROM Products";

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                ProductsDTO product = new ProductsDTO();
                product.setVendorno(rs.getInt("vendorno"));
                product.setProductcode(rs.getString("productcode"));
                product.setVendorsku(rs.getString("vendorsku"));
                product.setProductname(rs.getString("productname"));
                product.setCostprice(rs.getDouble("costprice"));
                product.setMSRP(rs.getDouble("msrp"));
                product.setRop(rs.getInt("rop"));
                product.setEoq(rs.getInt("eoq"));
                product.setQoh(rs.getInt("qoh"));
                product.setQoo(rs.getInt("qoo"));

                productArray.add(product);
            }

            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return productArray;
    }

    /* getProduct - retrieves a single product item from the database
     @param an int, a DataSource and a ResultSet
     @return a product of productsDTO
    */
    public ProductsDTO getProduct(int vendno, DataSource ds, ResultSet rs) {
        PreparedStatement pstmt;
        Connection con = null;
        ProductsDTO product = new ProductsDTO();

        String sql = "SELECT * FROM Products WHERE vendorno = " + vendno;

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

           rs.next();
                
               
                product.setProductcode(rs.getString("productcode"));
                product.setVendorsku(rs.getString("vendorsku"));
                product.setProductname(rs.getString("productname"));
                product.setCostprice(rs.getDouble("costprice"));
                product.setMSRP(rs.getDouble("msrp"));
                product.setRop(rs.getInt("rop"));
                product.setEoq(rs.getInt("eoq"));
                product.setQoh(rs.getInt("qoh"));
                product.setQoo(rs.getInt("qoo"));
                
            
           
            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return product;
    }
    
    /* getAllProductsForVendor - retrieves all product items from the database
     @param an int, a DataSource 
     @return an arrayList of type ProductsDTO
    */
    public ArrayList<ProductsDTO> getAllProductsForVendor(int vendno, DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        ArrayList<ProductsDTO> productArray = new ArrayList<>();

        String sql = "SELECT * FROM Products WHERE vendorno = " + vendno;

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                ProductsDTO product = new ProductsDTO();
                product.setVendorno(rs.getInt("vendorno"));
                product.setProductcode(rs.getString("productcode"));
                product.setVendorsku(rs.getString("vendorsku"));
                product.setProductname(rs.getString("productname"));
                product.setCostprice(rs.getDouble("costprice"));
                product.setMSRP(rs.getDouble("msrp"));
                product.setRop(rs.getInt("rop"));
                product.setEoq(rs.getInt("eoq"));
                product.setQoh(rs.getInt("qoh"));
                product.setQoo(rs.getInt("qoo"));

                productArray.add(product);
            }

            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return productArray;
    }
    
    /* updateProduct - updates a product item in the database
     @param a ProductsDTO, a DataSource 
     @return a string containing a msg
    */
    public String updateProduct(ProductsDTO p, DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        String msg = "";

        String sql = "UPDATE Products SET Vendorno = ?, Productcode = ?, Vendorsku = ?, Productname = ?, "
                + "Costprice = ?, MSRP = ?, Rop = ?, Eoq = ?, Qoh = ?, Qoo = ? WHERE productcode = ?";

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, p.getVendorno());
            pstmt.setString(2, p.getProductcode());
            pstmt.setString(3, p.getVendorsku());
            pstmt.setString(4, p.getProductname());
            pstmt.setDouble(5, p.getCostprice());
            pstmt.setDouble(6, p.getMSRP());
            pstmt.setInt(7, p.getRop());
            pstmt.setInt(8, p.getEoq());
            pstmt.setInt(9, p.getQoh());
            pstmt.setInt(10, p.getQoo());
            pstmt.setString(11, p.getProductcode());
            pstmt.executeUpdate();

            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }

        return msg;
    }

    /* deleteProduct - deletes a product item in the database
     @param a string, a DataSource 
     @return a string containing a msg
    */
    public String deleteProduct(String prodno, DataSource ds) {

        Connection con = null;
        PreparedStatement stmt;
        String productmsg = "";
        try {
            con = ds.getConnection();

            stmt = con.prepareStatement("DELETE FROM Products WHERE productcode = ?");
            stmt.setString(1, prodno);
            stmt.execute();

            stmt.close();
            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return productmsg;
    }

}

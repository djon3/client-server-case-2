package models;

import dtos.ProductsDTO;
import dtos.PurchaseOrderDTO;
import dtos.PurchaseOrderLineitemDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

/*
 * PurchaseOrderModel.java
 *
 *  Purpose:    Contains methods for supporting db access for purchase order information
 *              Usually consumed by the ViewModel Class via DTO
 *  Author:     Jon Decher
 *  Revisions:  None
 */
@Named(value = "purchaseOrderModel")
@RequestScoped
public class PurchaseOrderModel implements Serializable {

    public PurchaseOrderModel() {
    }

    /* getPOItems - retrieves purchase order items from database
     @param an int, an int and a DataSource
     @return an arraylist of PurchaseOrderLineitmDTO
    */
    public ArrayList<PurchaseOrderLineitemDTO> getPOItems(int vno, int ponum, DataSource ds) {
        String sql = "SELECT * FROM PurchaseOrderLineItems WHERE PONumber = ?";
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        ArrayList<PurchaseOrderLineitemDTO> items = new ArrayList();
        String msg = "";

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, ponum);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                PurchaseOrderLineitemDTO item = new PurchaseOrderLineitemDTO();
                //item.setProductCode(rs.getString("prodcd"));
                ProductsModal prm = new ProductsModal();
                ProductsDTO prodDTO = prm.getProduct(vno, ds, rs); //
                item.setProductCode(prodDTO.getProductcode());
                item.setProductname(prodDTO.getProductname());
                //item.setProductCode(rs.getString("prodcd"));
                //item.setProductname(rs.getString("prodname"));                
                item.setQty(rs.getInt("qty"));
                item.setEx(rs.getBigDecimal("price"));
                BigDecimal q = item.getEx().multiply(new BigDecimal(item.getQty()));
                item.setExtPrice(q);
                items.add(item);
            }
            pstmt.close();
            rs.close();
            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
            msg = "Error with getPOItems - " + se.getMessage();

        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }
        }
        return items;
    }

    /* getPO - retrieves purchase order from database
     @param an int and a DataSource
     @return a PurchaseOrderLineitmDTO
    */
    public PurchaseOrderDTO getPO(int ponum, DataSource ds) {
        String sql = "SELECT * FROM PurchaseOrders WHERE PONumber = ?";
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        PurchaseOrderDTO details = new PurchaseOrderDTO();

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, ponum);
            rs = pstmt.executeQuery();
            rs.next();
            details.setPonum(ponum);
            details.setAmount(rs.getBigDecimal("amount"));
            details.setPodate(Date.valueOf(rs.getString("podate")));
            // details.setVendorno(rs.getInt("vendorno"));          
            pstmt.close();
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }
        }

        return details;
    }

    /* purchaseOrderAdd - inserts into values into the purchase order table in the database
     @param a BigDecimal and an int
     @return a string
    */
    public String purchaseOrderAdd(BigDecimal total, int vendorno,
            ArrayList<PurchaseOrderLineitemDTO> items,
            DataSource ds) {
        Connection con = null;
        PreparedStatement pstmt;
        int poNum = -1;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
        String curDate = sdf.format(cal.getTime());
        String msg = "";
        String sql = "INSERT INTO PurchaseOrders (vendorno, podate, amount) VALUES (?, ?, ?)";
        double poamt = 1.13; //total.doubleValue() *

        try {
            con = ds.getConnection();
            con.setAutoCommit(false); //needed for trans rollback
            pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, vendorno);
            pstmt.setString(2, curDate);
            pstmt.setBigDecimal(3, BigDecimal.valueOf(poamt));
            pstmt.execute();
            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                rs.next();
                poNum = rs.getInt(1);
            }
            pstmt.close();

            for (PurchaseOrderLineitemDTO i : items) {

                if (i.getQty() > 0) {
                    sql = "INSERT INTO PurchaseOrderLineItems (PONumber, Prodcd, Qty, Price) VALUES (?, ?, ?, ?)";
                    pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
                    pstmt.setInt(1, poNum);
                    pstmt.setString(2, i.getProductCode());
                    pstmt.setInt(3, i.getQty());
                    pstmt.setBigDecimal(4, i.getEx());
                    pstmt.execute();
                }

            }//for

            con.commit();
            msg = "PO " + poNum + " Added!";
            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
            msg = "PO not added! - " + se.getMessage();
            try {
                con.rollback();
            } catch (SQLException sqx) {
                System.out.println("Rollback failed - " + sqx.getMessage());
            }
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }
        }
        return msg;
    }//purchaseOrderAdd

}

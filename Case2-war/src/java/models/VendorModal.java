package models;

import dtos.VendorDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

/*
 * VendorModal.java
 *
 *  Purpose:    Contains methods for supporting db access for vendor information
 *              Usually consumed by the ViewModel Class via DTO
 *  Author:     Jon Decher
 *  Revisions: 
 */
@Named(value = "vendorModal")
@RequestScoped
public class VendorModal implements Serializable {

    public VendorModal() {
    }

    /* addVendor - inserts values into the vendor table in the database
     @param a VendorsDTO and a DataSource
     @return an int containing the vendor number
    */
    public int addVendor(VendorDTO details, DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        int vendorno = 0;

        String sql = "INSERT INTO Vendors (Address1,City,Province,PostalCode,"
                + "Phone,VendorType,Name,Email) "
                + " VALUES (?,?,?,?,?,?,?,?)";

        try {
            System.out.println("before get connection");
            con = ds.getConnection();
            System.out.println("after get connection");
            pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, details.getAddress1());
            pstmt.setString(2, details.getCity());
            pstmt.setString(3, details.getProvince());
            pstmt.setString(4, details.getPostalCode());
            pstmt.setString(5, details.getPhone());
            pstmt.setString(6, details.getType());
            pstmt.setString(7, details.getName());
            pstmt.setString(8, details.getEmail());
            pstmt.execute();
           

            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                rs.next();
                vendorno = rs.getInt(1);
            }
            con.close();

        
        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return vendorno;
    }

    /* getVendors - retrieves vendor items from the database
     @param a DataSource
     @return an arraylist of VendorDTO
    */
    public ArrayList<VendorDTO> getVendors(DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        ArrayList<VendorDTO> vendorArray = new ArrayList<>();

        String sql = "SELECT * FROM Vendors";

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                VendorDTO vendor = new VendorDTO();
                vendor.setVendorno(rs.getInt("vendorno"));
                vendor.setName(rs.getString("name"));
                vendor.setAddress1(rs.getString("address1"));
                vendor.setCity(rs.getString("city"));
                vendor.setProvince(rs.getString("province"));
                vendor.setPostalCode(rs.getString("postalcode"));
                vendor.setPhone(rs.getString("phone"));
                vendor.setType(rs.getString("vendortype"));
                vendor.setEmail(rs.getString("email"));
                vendorArray.add(vendor);
            }

            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return vendorArray;
    }
           
    /* updateVendor - updates a vendor item in the database
     @param a VendorDTO, a DataSource 
     @return an int containing the number of row
    */
    public int updateVendor(VendorDTO ven, DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        int numOfRows = -1;

        String sql = "UPDATE Vendors SET Address1 = ?, City = ?, Province = ?, "
                + "PostalCode = ?, Phone = ?, VendorType = ?, Name = ?, "
                + "Email = ? WHERE vendorno = ?";

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, ven.getAddress1());
            pstmt.setString(2, ven.getCity());
            pstmt.setString(3, ven.getProvince());
            pstmt.setString(4, ven.getPostalCode());
            pstmt.setString(5, ven.getPhone());
            pstmt.setString(6, ven.getType());
            pstmt.setString(7, ven.getName());
            pstmt.setString(8, ven.getEmail());
            pstmt.setInt(9, ven.getVendorno());
            numOfRows = pstmt.executeUpdate();

            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }

        return numOfRows;
    }

    /* deleteVendor - deletes a vendor item in the database
     @param an int, a DataSource 
     @return an int containing the vendorno
    */
    public int deleteVendor(int vendno, DataSource ds) {

        Connection con = null;
        PreparedStatement stmt;
        int vendorno = 0;
        try {
            con = ds.getConnection();

            stmt = con.prepareStatement("DELETE FROM Vendors WHERE vendorno = ?");
            stmt.setInt(1, vendno);
            stmt.execute();

            stmt.close();
            con.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return vendorno;
    }

}

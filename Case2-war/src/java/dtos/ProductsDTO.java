package dtos;

/**
 * ProductsDTO - Container class that serializes product information traveling
 * between ViewModel and Model classes
 */
import java.io.Serializable;

public class ProductsDTO implements Serializable {

    public ProductsDTO() {
    }
    
    private String productcode;
    private int    vendorno;
    private String vendorsku;
    private String productname;
    private double costprice;
    private double msrp;
    private int    rop;
    private int    eoq;
    private int    qoh;
    private int    qoo;

    public String getProductcode() {
        return this.productcode;
    }

    public void setProductcode(String inValue) {
        this.productcode = inValue;
    }
    
    public int getVendorno() {
        return this.vendorno;
    }

    public void setVendorno(int inValue) {
        this.vendorno = inValue;
    }

    public String getVendorsku() {
        return this.vendorsku;
    }

    public void setVendorsku(String inValue) {
        this.vendorsku = inValue;
    }

    public String getProductname() {
        return this.productname;
    }

    public void setProductname(String inValue) {
        this.productname = inValue;
    }

    public double getCostprice() {
        return this.costprice;
    }

    public void setCostprice(double inValue) {
        this.costprice = inValue;
    }

    public double getMSRP() {
        return this.msrp;
    }

    public void setMSRP(double inValue) {
        this.msrp = inValue;
    }
    
    public int getRop() {
        return this.rop;
    }

    public void setRop(int inValue) {
        this.rop = inValue;
    }
    
    public int getEoq() {
        return this.eoq;
    }

    public void setEoq(int inValue) {
        this.eoq = inValue;
    }
    
    public int getQoh() {
        return this.qoh;
    }

    public void setQoh(int inValue) {
        this.qoh = inValue;
    }

    public int getQoo() {
        return this.qoo;
    }

    public void setQoo(int inValue) {
        this.qoo = inValue;
    }
    

   
}

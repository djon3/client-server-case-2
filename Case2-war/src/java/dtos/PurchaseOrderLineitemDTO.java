

package dtos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * PurchaseOrderLineitemDTO - Container class that serializes purchase order line item information traveling
 * between ViewModel and Model classes
 */

public class PurchaseOrderLineitemDTO implements Serializable  {
        
    
    public PurchaseOrderLineitemDTO(){}
    
    private int lineid;
    private int ponum;   
    private String prodname;
    private String prodcd = ".";   
    private int qty;
    private BigDecimal ex;
    private BigDecimal extprice;
    
    public int getLineid() {
        return this.lineid;
    }

    public void setLineid(int inValue) {
        this.lineid = inValue;
    }   
    
    public int getPonum() {
        return this.ponum;
    }

    public void setPonum(int inValue) {
        this.ponum = inValue;
    }
    
    public String getProductname() {        
        return this.prodname;
    }

    public void setProductname(String inValue) {
        this.prodname = inValue;
    }
    
    public String getProductCode() {        
        return this.prodcd;
    }

    public void setProductCode(String inValue) {
        this.prodcd = inValue;
    }
    
    public int getQty() {
        return this.qty;
    }

    public void setQty(int inValue) {
        this.qty = inValue;
    }
    
    public BigDecimal getEx() {
        return this.ex;
    }

    public void setEx(BigDecimal inValue) {
        this.ex = inValue;
    }
    
    public BigDecimal getExtPrice() {
        return this.extprice;
    }

    public void setExtPrice(BigDecimal inValue) {
        this.extprice = inValue;
    }
    
}

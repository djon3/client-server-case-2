/**
 * PurchaseOrderDTO - Container class that serializes purchase orders information traveling
 * between ViewModel and Model classes
 */

package dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author djon
 */


public class PurchaseOrderDTO implements Serializable {
    
    public PurchaseOrderDTO(){}
    
    private int ponum;
    private int vendorno;
    private BigDecimal total;
    private Date podate;
   
    private ArrayList<PurchaseOrderLineitemDTO> items ;
 
     public int getPonum() {
        return this.ponum;
    }

    public void setPonum(int inValue) {
        this.ponum = inValue;
    }
    
    public int getVendorno() {
        return this.vendorno;
    }

    public void setVendorno(int inValue) {
        this.vendorno = inValue;
    }
    
    public BigDecimal getAmount() {
        return this.total;
    }

    public void setAmount(BigDecimal inValue) {
        this.total = inValue;
    }
 
    public Date getPodate() {
        return this.podate;
    }

    public void setPodate(Date inValue) {
        this.podate= inValue;
    }
    
    public ArrayList<PurchaseOrderLineitemDTO> getItems() {
        return items;
    }
    
        public void setItems(ArrayList<PurchaseOrderLineitemDTO> inValue) {
        this.items = inValue;
    }


    

    
}

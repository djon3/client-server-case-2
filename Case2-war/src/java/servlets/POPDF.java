package servlets;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dtos.PurchaseOrderDTO;
import dtos.PurchaseOrderLineitemDTO;
import dtos.VendorDTO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import models.PurchaseOrderModel;
import models.VendorModal;

/**
 *
 * @author Jonathan
 */
@WebServlet(name = "POPDFServlet", urlPatterns = {"/POPDF"})
public class POPDF extends HttpServlet {

    @Resource(lookup = "jdbc/Info5059db")
    DataSource ds;
    PurchaseOrderModel pom = new PurchaseOrderModel();
    VendorModal vm = new VendorModal();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int pono = Integer.parseInt(request.getParameter("po"));
            PurchaseOrderDTO poDTO = pom.getPO(pono, ds);
            ArrayList<VendorDTO> vDTO = vm.getVendors(ds);
            int vno = vDTO.get(0).getVendorno();
            ArrayList<PurchaseOrderLineitemDTO> poliDTO = pom.getPOItems(vno, pono, ds);
            buildpdf(poDTO, vDTO, poliDTO, response);

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }

    private void buildpdf(PurchaseOrderDTO poDTO,
            ArrayList<VendorDTO> vDTO,
            ArrayList<PurchaseOrderLineitemDTO> poliDTO,
            HttpServletResponse response) {
        Font catFont = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
        Font subFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Font smallFont = new Font(Font.FontFamily.HELVETICA, 12);

        String IMG = getServletContext().getRealPath("/img/logo.png");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Document document = new Document();

        try {
            PdfWriter.getInstance(document, baos);
            document.open();
            Paragraph preface = new Paragraph();
            // We add one empty line
            Image image1 = Image.getInstance(IMG);
            image1.scaleAbsolute(125, 125);
            image1.setAbsolutePosition(55f, 700f);
            preface.add(image1);
            preface.setAlignment(Element.ALIGN_RIGHT);
            // Lets write a big header
            Paragraph mainHead = new Paragraph(String.format("%55s", "Purchase Order"), catFont);
            preface.add(mainHead);
            preface.add(new Paragraph(String.format("%82s", "PO#: " + poDTO.getPonum()), subFont));
            addEmptyLine(preface, 2);

            PdfPTable vendorTable = new PdfPTable(7);

            PdfPCell vendorCell = new PdfPCell(new Paragraph("Vendor: ", smallBold));
            vendorCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            vendorCell.setBorder(0);
            vendorTable.addCell(vendorCell);
            vendorCell = new PdfPCell(new Paragraph(String.format(vDTO.get(poDTO.getVendorno()).getName()
                    + "\n" + vDTO.get(poDTO.getVendorno()).getAddress1()
                    + "\n" + vDTO.get(poDTO.getVendorno()).getCity()
                    + "\n" + vDTO.get(poDTO.getVendorno()).getProvince()
                    + "\n" + vDTO.get(poDTO.getVendorno()).getPostalCode()), smallFont));
            vendorCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            vendorCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            vendorCell.setBorder(0);
            vendorTable.addCell(vendorCell);

            //empty cells   
            vendorCell = new PdfPCell(new Paragraph("", smallBold));
            vendorCell.setBorder(0);   
            vendorTable.addCell(vendorCell);             
            vendorCell = new PdfPCell(new Paragraph("", smallBold));
            vendorCell.setBorder(0);   
            vendorTable.addCell(vendorCell);            
            vendorCell = new PdfPCell(new Paragraph("", smallBold));
            vendorCell.setBorder(0);   
            vendorTable.addCell(vendorCell);
            vendorCell = new PdfPCell(new Paragraph("", smallBold));
            vendorCell.setBorder(0); 
            vendorTable.addCell(vendorCell);
            vendorCell = new PdfPCell(new Paragraph("", smallBold));
            vendorCell.setBorder(0);
            vendorTable.addCell(vendorCell);

            preface.add(vendorTable);

            addEmptyLine(preface, 2);

            // 5 column table
            PdfPTable table = new PdfPTable(5);
            PdfPCell cell = new PdfPCell(new Paragraph("Product Code", smallBold));
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Product Description", smallBold));
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Quantity Sold", smallBold));
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Price", smallBold));
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Ext. Price", smallBold));
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            
            BigDecimal totalPrice = new BigDecimal(0);
            
            for(int i = 0; i < poliDTO.size(); ++i){
                cell = new PdfPCell(new Phrase(poliDTO.get(i).getProductCode()));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(poliDTO.get(i).getProductname()));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("" + poliDTO.get(i).getQty()));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("$" + poliDTO.get(i).getEx()));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("$" + poliDTO.get(i).getExtPrice()));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                              
                totalPrice = totalPrice.add(poliDTO.get(i).getExtPrice());
            }

            cell = new PdfPCell(new Phrase("Total:"));
            cell.setColspan(4);
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("$" + totalPrice));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Sub:"));
            cell.setColspan(4);
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);            
            BigDecimal sub = totalPrice.multiply(new BigDecimal(0.13));   
            cell = new PdfPCell(new Phrase("$" + (sub.setScale(2, RoundingMode.CEILING))));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Order Total:"));
            cell.setColspan(4);
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            BigDecimal orderTotal = (totalPrice.add(sub)).setScale(2, RoundingMode.CEILING);
            cell = new PdfPCell(new Phrase("$" + orderTotal));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(BaseColor.YELLOW);
            table.addCell(cell);
            
            preface.add(table);
            addEmptyLine(preface, 3);
            preface.setAlignment(Element.ALIGN_CENTER);
            preface.add(new Paragraph(String.format("%60s", new Date()), smallFont));
            document.add(preface);
            document.close();

            // setting some response headers
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            // setting the content type
            response.setContentType("application/pdf");
            // the contentlength
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }

    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

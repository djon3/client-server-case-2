package case2ejbs;

import dtos.ProductEJBDTO;
import dtos.PurchaseOrderEJBDTO;
import dtos.PurchaseOrderLineitemEJBDTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import models.ProductsModel;
import models.PurchaseorderlineitemsModel;
import models.PurchaseordersModel;
import models.VendorsModel;

/**
 *
 * @author djon
 */
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Stateless
@LocalBean
public class POFacadeBean {

    @PersistenceContext(unitName = "Case2-ejbPU")
    private EntityManager em;

    @Resource
    private EJBContext context;

    /**
     * addPO - receive complete PO data from DTO, add the PO row first then the
     * line items using JPA. Also, JTA used for transaction management.
     *
     * @param poDTO
     * @return PO# added
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String addPO(PurchaseOrderEJBDTO poDTO) {
        PurchaseordersModel pm;
        VendorsModel vm;
        String msg = "";
        Date poDate = new java.util.Date();

        try {
            vm = em.find(VendorsModel.class, poDTO.getVendorno());
            pm = new PurchaseordersModel(0, poDTO.getAmount(), poDate);
            pm.setVendorno(vm);
            em.persist(pm);
            em.flush();
            int poRowID = pm.getPonumber();

            for (PurchaseOrderLineitemEJBDTO line : poDTO.getItems()) {
                if (line.getQty() > 0) {
                    int retVal = addPOLine(line, pm);
                    if(retVal < 0){
                        line.setPonum(line.getPonum());                        
                        line.setProductCode(line.getProductCode());                        
                        line.setQty(line.getQty());
                        line.setEx(line.getEx());
                        em.persist(pm);
                        em.flush();
                    }

                }
            }

            msg = "PO " + poRowID + " Added!";

        } catch (ConstraintViolationException v) {
            Set<ConstraintViolation<?>> coll = v.getConstraintViolations();
            for (ConstraintViolation s : coll) {
                System.out.println(s.getPropertyPath() + " " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            context.setRollbackOnly();
        }

        return msg;
    }

    /**
     * addPOLine - receive complete PO data from DTO and data from the Purchase 
     * orders model. Adds the line items using JPA. Also, JTA used for transaction management.
     * @param line
     * @param pom
     * @return 
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private int addPOLine(PurchaseOrderLineitemEJBDTO line, PurchaseordersModel pom) {
        PurchaseorderlineitemsModel polm;
        int retVal = -1;
        try {
            int rowsUpdated = updateInventory(line.getProductCode(), line.getQty());
            polm = new PurchaseorderlineitemsModel(0, line.getProductCode(), line.getQty(), line.getEx());
            polm.setPonumber(pom);
            em.persist(polm);
            em.flush();
            retVal = polm.getLineid();
        } catch (ConstraintViolationException v) {
            Set<ConstraintViolation<?>> coll = v.getConstraintViolations();
            for (ConstraintViolation s : coll) {
                System.out.println(s.getPropertyPath() + " " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            context.setRollbackOnly();
        }

        return retVal;
    }

    /**
     * updateInventory - receives the productcode and qty and updates the QOO of
     * the selected product in according to the users qty input.
     * @param productcode
     * @param qty
     * @return 
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private int updateInventory(String productcode, int qty) {
        ProductsModel prdm;
        int rowsUpdated = -1;
        try {
            prdm = em.find(ProductsModel.class, productcode);
            prdm.setQoo(prdm.getQoo() + qty);
            em.persist(prdm);
            rowsUpdated = 1;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            context.setRollbackOnly();
            throw e;
        }
        return rowsUpdated;
    }
    
    /**
     * getAllPOsForVendor - receives a vendor number and returns all purchase orders
     * for that vendor.
     * @param vendorno
     * @return 
     */
    public ArrayList<PurchaseOrderEJBDTO> getAllPOsForVendor(int vendorno){
        ArrayList<PurchaseOrderEJBDTO> poDTO = new ArrayList<>();
        List<PurchaseordersModel> pos;        
       
        try{
             Query qry = em.createNamedQuery("PurchaseordersModel.findAll");
           
            pos = qry.getResultList();
            for (PurchaseordersModel p : pos) {
                PurchaseOrderEJBDTO dto = new PurchaseOrderEJBDTO();
                dto.setPonum(p.getPonumber());
                dto.setAmount(p.getAmount());
                dto.setPodate(p.getPodate());
                poDTO.add(dto);
            }
            
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return poDTO;
    }
    
    public ArrayList<PurchaseOrderLineitemEJBDTO> getAllPOLIsForVendor(){
        ArrayList<PurchaseOrderLineitemEJBDTO> poDTO = new ArrayList<>();
        List<PurchaseorderlineitemsModel> pos;        
       
        try{
             Query qry = em.createNamedQuery("PurchaseorderlineitemsModel.findAll");
           
            pos = qry.getResultList();
            for (PurchaseorderlineitemsModel p : pos) {
                PurchaseOrderLineitemEJBDTO dto = new PurchaseOrderLineitemEJBDTO();
                dto.setProductCode(p.getProdcd());
                dto.setProductname(p.getProdcd());
                dto.setQty(p.getQty());
                dto.setExtPrice(p.getPrice());
                poDTO.add(dto);
            }
            
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return poDTO;
    }

}

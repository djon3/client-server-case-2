/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package case2ejbs;

import dtos.ProductEJBDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import models.ProductsModel;
import models.VendorsModel;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author djon
 */
@Stateless
@LocalBean
public class ProductFacadeBeans {

    @PersistenceContext(unitName = "Case2-ejbPU")
    private EntityManager em;

    /* addProduct - inserts values into the product table in the database
     @param a ProductsDTO and a DataSource
     @return an int containing the vendor number
     */
    public int addProduct(ProductEJBDTO dto) {
        ProductsModel pm;
        VendorsModel vm;
        int rowsAdded = -1;
        byte[] qrcodebin;
       
        try {
            qrcodebin = QRCode.from(dto.getQrcodetxt()).to(ImageType.PNG).stream().toByteArray();

            pm = new ProductsModel(dto.getProductcode(),
                    dto.getVendorsku(), dto.getProductname(), dto.getCostprice(),
                    dto.getMSRP(), dto.getRop(), dto.getEoq(), dto.getQoh(),
                    dto.getQoo(), dto.getQrcodetxt(), qrcodebin);
            vm = em.find(VendorsModel.class, dto.getVendorno());
            pm.setVendorno(vm);
            em.persist(pm);
            em.flush();
            rowsAdded = 1;
        } catch (ConstraintViolationException v) {
            Set<ConstraintViolation<?>> coll = v.getConstraintViolations();
            for (ConstraintViolation s : coll) {
                System.out.println(s.getPropertyPath() + " " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return rowsAdded;
    }

    /* getProducts - retrieves all product items from the database     
     @return an arraylist of ProductEJBDTO
     */
    public ArrayList<ProductEJBDTO> getAllProducts() {
        ArrayList<ProductEJBDTO> productsDTO = new ArrayList<>();
        List<ProductsModel> products;

        try {
            Query qry = em.createNamedQuery("ProductsModel.findAll");
            products = qry.getResultList();

            for (ProductsModel p : products) {
                ProductEJBDTO dto = new ProductEJBDTO();
                dto.setProductcode(p.getProductcode());
                dto.setCostprice(p.getCostprice());
                dto.setEoq(p.getEoq());
                dto.setMSRP(p.getMsrp());
                dto.setProductname(p.getProductname());
                dto.setQoh(p.getQoh());
                dto.setQoo(p.getQoo());
                dto.setQrcodetxt(p.getQrcodetxt());
                dto.setRop(p.getRop());
                dto.setVendorno(p.getVendorno().getVendorno());
                dto.setVendorsku(p.getVendorsku());
                productsDTO.add(dto);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return productsDTO;
    }

    /* getProduct - retrieves a single product item from the database
     @param a productcode string 
     @return a product of ProductsEJBDTO    
     */
    public ProductEJBDTO getProduct(String productcode) {
               
        ProductEJBDTO dto = new ProductEJBDTO();
        List<ProductsModel> products;
        try {                              
            Query qry = em.createNamedQuery("ProductsModel.findAll");
            products = qry.getResultList();

            for (ProductsModel p : products) {
                if(p.getQrcodetxt().equals(productcode)){
                    dto.setProductcode(p.getProductcode());
                    dto.setCostprice(p.getCostprice());
                    dto.setEoq(p.getEoq());
                    dto.setMSRP(p.getMsrp());
                    dto.setProductname(p.getProductname());
                    dto.setQoh(p.getQoh());
                    dto.setQoo(p.getQoo());
                    dto.setQrcode((byte[])p.getQrcode());
                    dto.setQrcodetxt(p.getQrcodetxt());
                    dto.setRop(p.getRop());
                    dto.setVendorno(p.getVendorno().getVendorno());
                    dto.setVendorsku(p.getVendorsku());
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return dto;
    }
    
    public ArrayList<ProductEJBDTO> getAllProductsForVendor(int vendorno){
        ArrayList<ProductEJBDTO> productsDTO = new ArrayList<>();
        List<ProductsModel> products;
        VendorsModel vm;
        
        try{
            vm = em.find(VendorsModel.class, vendorno);
            Query qry = em.createNamedQuery("ProductsModel.findByVendorno");
            qry.setParameter("vendorno", vm);
            products = qry.getResultList();
            for (ProductsModel p : products) {
                ProductEJBDTO dto = new ProductEJBDTO();
                dto.setProductcode(p.getProductcode());
                dto.setCostprice(p.getCostprice());
                dto.setEoq(p.getEoq());
                dto.setMSRP(p.getMsrp());
                dto.setProductname(p.getProductname());
                dto.setQoh(p.getQoh());
                dto.setQoo(p.getQoo());
                dto.setQrcodetxt(p.getQrcodetxt());
                dto.setRop(p.getRop());
                dto.setVendorno(p.getVendorno().getVendorno());
                dto.setVendorsku(p.getVendorsku());
                productsDTO.add(dto);
            }
            
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }

        
        return productsDTO;
    }


    /* updateProduct - updates a product item in the database
     @param dto of ProductsEJBDTO
     @return a string containing a msg of the updated product
     */
    public String updateProduct(ProductEJBDTO dto) {
        ProductsModel pm;
        VendorsModel vm;        
        String msg = "";

        try {
            pm = em.find(ProductsModel.class, dto.getProductcode());
            vm = em.find(VendorsModel.class, dto.getVendorno());
            pm.setVendorno(vm);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return msg;
    }

    /* deleteProduct - deletes a product item in the database
     @param a string of the productcode
     @return a string containing a msg
     */
    public String deleteProduct(String productcode) {
        ProductsModel pm;
        String msg = "";

        try {
            pm = em.find(ProductsModel.class, productcode);
            em.remove(pm);
            em.flush();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return msg;
    }
   
}

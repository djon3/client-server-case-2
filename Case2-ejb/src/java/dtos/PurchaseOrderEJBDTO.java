/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author djon
 */
public class PurchaseOrderEJBDTO implements Serializable{
    
   public PurchaseOrderEJBDTO(){}
    
    private int ponum;
    private int vendorno;
    private BigDecimal amount;
    private Date podate;
   
    private ArrayList<PurchaseOrderLineitemEJBDTO> items ;
 
     public int getPonum() {
        return this.ponum;
    }

    public void setPonum(int inValue) {
        this.ponum = inValue;
    }
    
    public int getVendorno() {
        return this.vendorno;
    }

    public void setVendorno(int inValue) {
        this.vendorno = inValue;
    }
    
    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal inValue) {
        this.amount = inValue;
    }
 
    public Date getPodate() {
        return this.podate;
    }

    public void setPodate(Date inValue) {
        this.podate= inValue;
    }
    
    public ArrayList<PurchaseOrderLineitemEJBDTO> getItems() {
        return items;
    }
    
        public void setItems(ArrayList<PurchaseOrderLineitemEJBDTO> inValue) {
        this.items = inValue;
    }
    
    
}

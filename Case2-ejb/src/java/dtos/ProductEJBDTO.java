/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dtos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author djon
 */

public class ProductEJBDTO implements Serializable {
    
    public ProductEJBDTO(){}
    
    private String productcode;
    private int    vendorno;
    private String vendorsku;
    private String productname;
    private BigDecimal costprice;
    private BigDecimal msrp;
    private int    rop;
    private int    eoq;
    private int    qoh;
    private int    qoo;
    private String qrcodetxt;
    private byte[] qrcodebin;

    public String getProductcode() {
        return this.productcode;
    }

    public void setProductcode(String inValue) {
        this.productcode = inValue;
    }
    
    public int getVendorno() {
        return this.vendorno;
    }

    public void setVendorno(int inValue) {
        this.vendorno = inValue;
    }

    public String getVendorsku() {
        return this.vendorsku;
    }

    public void setVendorsku(String inValue) {
        this.vendorsku = inValue;
    }

    public String getProductname() {
        return this.productname;
    }

    public void setProductname(String inValue) {
        this.productname = inValue;
    }

    public BigDecimal getCostprice() {
        return this.costprice;
    }

    public void setCostprice(BigDecimal inValue) {
        this.costprice = inValue;
    }

    public BigDecimal getMSRP() {
        return this.msrp;
    }

    public void setMSRP(BigDecimal inValue) {
        this.msrp = inValue;
    }
    
    public int getRop() {
        return this.rop;
    }

    public void setRop(int inValue) {
        this.rop = inValue;
    }
    
    public int getEoq() {
        return this.eoq;
    }

    public void setEoq(int inValue) {
        this.eoq = inValue;
    }
    
    public int getQoh() {
        return this.qoh;
    }

    public void setQoh(int inValue) {
        this.qoh = inValue;
    }

    public int getQoo() {
        return this.qoo;
    }

    public void setQoo(int inValue) {
        this.qoo = inValue;
    }
    
    public String getQrcodetxt() {
        return this.qrcodetxt;
    }

    public void setQrcodetxt(String inValue) {
        this.qrcodetxt = inValue;
    }
    
    public byte[] getQrcode() {
        return this.qrcodebin;
    }

    public void setQrcode(byte[] inValue) {
        this.qrcodebin = inValue;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dtos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author djon
 */
public class PurchaseOrderLineitemEJBDTO implements Serializable  {
        
    
    public PurchaseOrderLineitemEJBDTO(){}
    
    private int lineid;
    private int ponum;   
    private String prodname;
    private String productCode ;   
    private int qty;
    private BigDecimal ex;
    private BigDecimal extprice;
    
    public int getLineid() {
        return this.lineid;
    }

    public void setLineid(int inValue) {
        this.lineid = inValue;
    }   
    
    public int getPonum() {
        return this.ponum;
    }

    public void setPonum(int inValue) {
        this.ponum = inValue;
    }
    
    public String getProductname() {        
        return this.prodname;
    }

    public void setProductname(String inValue) {
        this.prodname = inValue;
    }
    
    public String getProductCode() {        
        return this.productCode;
    }

    public void setProductCode(String inValue) {
        this.productCode = inValue;
    }
    
    public int getQty() {
        return this.qty;
    }

    public void setQty(int inValue) {
        this.qty = inValue;
    }
    
    public BigDecimal getEx() {
        return this.ex;
    }

    public void setEx(BigDecimal inValue) {
        this.ex = inValue;
    }
    
    public BigDecimal getExtPrice() {
        return this.extprice;
    }

    public void setExtPrice(BigDecimal inValue) {
        this.extprice = inValue;
    }
    
}
